1.  将内核源文件linux-xlnx-xilinx-v2017.4复制到linux主机，然后解压，解压后的内核目录就是本实验petalinux要用到的内核。打开终端，进入前面实验中的petalinux工程目录，运行命令source   /opt/pkg/petalinux/settings.sh设置petalinux环境变量，运行命令source  /opt/Xilinx/Vivado/2017.4/settings64.sh设置vivado环境变量，运行命令petalinux-config重新配置petalinux，配置完成后保存退出。
2.  运行命令petalinux-config  -c  kernel配置内核，配置完成后保存退出。
3.  打开petalinux工程文件中的名为system-user.dtsi文件，修改设备树的内容。
4.  运行命令petalinux-build来配置编译uboot、内核、根文件系统、设备树等。
5.  运行命令petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf  --fpga  --u-boot --force生成BOOT文件，将BOOT.bin和image.ub复制到sd中，设置开发板sd模式启动，插上HDMI显示器，启动开发板，显示器会显示出内容。